# Youtube
<details>
<summary>Center the video if possible</summary>

> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 1](styles/youtube.css#L1).

</details>
<details>
<summary>Remove some useless suggestion around the video description</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 11](styles/youtube.css#L11).

</details>
<details>
<summary>Remove trending and premium links</summary>

> « Any curation process should be made in the user's interest.
A platform curation process should only help the user find interesting content. This process, if not properly disclosed, signals that a conflict of interest might be at play. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 20](styles/youtube.css#L20).

</details>
<details>
<summary>Simplify youtube logo to make it more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 28](styles/youtube.css#L28).

</details>
<details>
<summary>Remove useless youtube appdrawer</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 37](styles/youtube.css#L37).

</details>
<details>
<summary>Remove right side video suggestions and eventual playlist sidebar (replaced by the playlist button on the top left corner of the video)</summary>

> « Any curation process should be made in the user's interest.
A platform curation process should only help the user find interesting content. This process, if not properly disclosed, signals that a conflict of interest might be at play. »  
> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 42](styles/youtube.css#L42).

</details>
<details>
<summary>Desaturate thumbnails and pictures</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 61](styles/youtube.css#L61).

</details>
<details>
<summary>Hide suggested channels. Only work when the channel owner already suggests channels</summary>

> « Any curation process should be made in the user's interest.
A platform curation process should only help the user find interesting content. This process, if not properly disclosed, signals that a conflict of interest might be at play. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 66](styles/youtube.css#L66).

</details>
<details>
<summary>Use a neutral color for the bottom video progress bar</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 71](styles/youtube.css#L71).

</details>
<details>
<summary>Use a neutral color for the subscribe button and remove sub-count from it</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 76](styles/youtube.css#L76).

</details>
<details>
<summary>Remove left side annotations.</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 91](styles/youtube.css#L91).

</details>
<details>
<summary>Remove bottom right video branding</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 95](styles/youtube.css#L95).

</details>
<details>
<summary>Remove the theater button</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 100](styles/youtube.css#L100).

</details>
<details>
<summary>Remove the minify button</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 105](styles/youtube.css#L105).

</details>
<details>
<summary>Remove the "next video" button when the video is not in a playlist</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »  
> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 110](styles/youtube.css#L110).

</details>
<details>
<summary>Make video title lowercase</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 118](styles/youtube.css#L118).

</details>
<details>
<summary>Use white for the background color of the navbar of the mobile website</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/youtube.css](styles/youtube.css) at [line 128](styles/youtube.css#L128).

</details>
<details>
<summary>Force theater mode</summary>

> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [scripts/youtube.js](scripts/youtube.js) at [line 1](scripts/youtube.js#L1).

</details>
<details>
<summary>Replace the subscription list from the side menu by a link to the subscription manager</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [scripts/youtube.js](scripts/youtube.js) at [line 11](scripts/youtube.js#L11).

</details>
<details>
<summary>Remove the trending page</summary>

> « Any curation process should be made in the user's interest.
A platform curation process should only help the user find interesting content. This process, if not properly disclosed, signals that a conflict of interest might be at play. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [scripts/background/youtube.js](scripts/background/youtube.js) at [line 2](scripts/background/youtube.js#L2).

</details>
<details>
<summary>Remove autoplay "feature"</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [scripts/background/youtube.js](scripts/background/youtube.js) at [line 14](scripts/background/youtube.js#L14).

</details>

# Facebook
<details>
<summary>Remove messaging sidebar and popups from all pages</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/facebook.css](styles/facebook.css) at [line 1](styles/facebook.css#L1).

</details>
<details>
<summary>Remove stories block</summary>

> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/facebook.css](styles/facebook.css) at [line 15](styles/facebook.css#L15).

</details>
<details>
<summary>Change the navbar color to match the background color</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/facebook.css](styles/facebook.css) at [line 22](styles/facebook.css#L22).

</details>
<details>
<summary>Change the navbar color to a neutral one on mobile</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/facebook.css](styles/facebook.css) at [line 38](styles/facebook.css#L38).

</details>
<details>
<summary>Remove unnecessary links in left column</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/facebook.css](styles/facebook.css) at [line 51](styles/facebook.css#L51).

</details>
<details>
<summary>Make legal links as visible as other elements in the right colum</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/facebook.css](styles/facebook.css) at [line 62](styles/facebook.css#L62).

</details>
<details>
<summary>Hide the textarea to create a post until you click on "create a post"</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/facebook.css](styles/facebook.css) at [line 69](styles/facebook.css#L69).

</details>
<details>
<summary>Remove messenger button on mobile browsers</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/facebook.css](styles/facebook.css) at [line 77](styles/facebook.css#L77).

</details>
<details>
<summary>Remove the install messenger mobile page</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [scripts/background/facebook.js](scripts/background/facebook.js) at [line 2](scripts/background/facebook.js#L2).

</details>

# Twitter
<details>
<summary>Tweet buttons are now neutral links</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/twitter.css](styles/twitter.css) at [line 1](styles/twitter.css#L1).

</details>
<details>
<summary>Twitter logo is more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/twitter.css](styles/twitter.css) at [line 13](styles/twitter.css#L13).

</details>

# Google
<details>
<summary>Make google logo more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/google.css](styles/google.css) at [line 1](styles/google.css#L1).

</details>
<details>
<summary>Use text instead of cards on mobile</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/google.css](styles/google.css) at [line 7](styles/google.css#L7).

</details>
<details>
<summary>Hide floating searchbar</summary>

> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/google.css](styles/google.css) at [line 21](styles/google.css#L21).

</details>

# Stackoverflow
<details>
<summary>Hide "featured" meta pannel, newsletter prompt, teams prompt</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/stackoverflow.css](styles/stackoverflow.css) at [line 1](styles/stackoverflow.css#L1).

</details>
<details>
<summary>Remove hot network questions</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/stackoverflow.css](styles/stackoverflow.css) at [line 7](styles/stackoverflow.css#L7).

</details>
<details>
<summary>Hide badges and reputation from topbar</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/stackoverflow.css](styles/stackoverflow.css) at [line 11](styles/stackoverflow.css#L11).

</details>
<details>
<summary>Remove left content border</summary>

> No existing rule from the [manifesto](MANIFESTO.md) has been defined for this change.  
> ___ 
> This change is implemented in [styles/stackoverflow.css](styles/stackoverflow.css) at [line 16](styles/stackoverflow.css#L16).

</details>
<details>
<summary>Make top bar's colors fit the background color</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/stackoverflow.css](styles/stackoverflow.css) at [line 21](styles/stackoverflow.css#L21).

</details>
<details>
<summary>Make the logo more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/stackoverflow.css](styles/stackoverflow.css) at [line 33](styles/stackoverflow.css#L33).

</details>
<details>
<summary>Make related questions colors less bright</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/stackoverflow.css](styles/stackoverflow.css) at [line 39](styles/stackoverflow.css#L39).

</details>
<details>
<summary>Make navigation colors neutral</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/stackoverflow.css](styles/stackoverflow.css) at [line 52](styles/stackoverflow.css#L52).

</details>

# Amazon
<details>
<summary>Remove main page overwhelming fullpage suggestion</summary>

> « The main content of the page should stay the only focus
The user should see the main content first and foremost when landing on a page. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 1](styles/amazon.css#L1).

</details>
<details>
<summary>Remove product page arbitrary suggestions</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 10](styles/amazon.css#L10).

</details>
<details>
<summary>Remove other futile suggestions</summary>

> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 15](styles/amazon.css#L15).

</details>
<details>
<summary>Use neutral colors for the navbar</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 21](styles/amazon.css#L21).

</details>
<details>
<summary>Make logo more discrete</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 61](styles/amazon.css#L61).

</details>
<details>
<summary>Make button colors neutral</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 70](styles/amazon.css#L70).

</details>
<details>
<summary>Desaturate orange titles</summary>

> « Logos and branding visuals should remain purely informative.
Colors and graphics must not be used to create some sort of pavlovian conditioning. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 75](styles/amazon.css#L75).

</details>
<details>
<summary>Remove the buy now button on desktop</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 80](styles/amazon.css#L80).

</details>
<details>
<summary>Remove the "Pay in x installments free of charge" prompt</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 84](styles/amazon.css#L84).

</details>
<details>
<summary>Remove availability information from the the description of the product</summary>

> « Additional content must be relevant.  
Displaying additional unasked information can be used to change the behavior of the user. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 88](styles/amazon.css#L88).

</details>
<details>
<summary>Remove sharing buttons from the product page</summary>

> « Shortcuts must link to related content or actions that are hard to access.
There is no need for shortcuts that invite to use services already easily accessible and/or not obviously linked to the main content. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 92](styles/amazon.css#L92).

</details>
<details>
<summary>Remove recently viewed products from a product page</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »  
> « There should be no suggestions that have no obvious link to the main content.
If a message is not strongly linked to the main content, it is an advertisement either for suggested content or for the platform, or both, and should be signaled as such. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 97](styles/amazon.css#L97).

</details>
<details>
<summary>Remove overwhelming pictures from navigation menus</summary>

> « Interactive elements should remain informative and unbiased.
Manipulative interactive elements prevent the user from making their own choices. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 102](styles/amazon.css#L102).

</details>
<details>
<summary>Remove the cart right sidebar</summary>

> « A page must only have one key purpose.
Other functions must be accessed by an action of the user, not forced onto them. »  
> *From the [manifesto](MANIFESTO.md).*  
> ___ 
> This change is implemented in [styles/amazon.css](styles/amazon.css) at [line 112](styles/amazon.css#L112).

</details>

