all: manifest.json clean zip

zip: 
	zip -r ./minimal.zip LICENSE README.md CHANGES.md manifest.json icons scripts styles

manifest.json: preprocessing/preprocess $(shell find data -type f) $(shell find scripts -type f) $(shell find styles -type f)
	./preprocessing/preprocess

.PHONY: ppmake

ppmake: preprocessing/preprocess

preprocessing/preprocess: 
	cd preprocessing && $(MAKE)

version:
	cat ./data/version

clean:
	$(RM) ./minimal.zip

ppclean:
	cd preprocessing && $(MAKE) clean
	
deepclean: clean ppclean
