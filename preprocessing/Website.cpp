#include "Website.h"

Website::Website(void) {}

Website::Website(std::string name) : name(name) {}

void Website::setName(std::string newName) {
  name = newName;
}

std::string Website::getName(void) {
  return name;
}

void Website::addTld(std::string tld) {
  tlds.push_back(tld);
}

std::string Website::printUrls(void) {
  std::stringstream urlList;
  for (auto const& tld: tlds) {
    urlList << "\"*://*." << name << "." << tld << "/*\"";
    if (tld != tlds.back()) {
      urlList << ", ";
    }
  }
  return urlList.str();
}

std::string Website::printElement(void) { // Used to fill content_scripts
  std::stringstream element;
  element << "\t\t{" << std::endl;
  element << "\t\t\t\"matches\": [" << printUrls() << "]," << std::endl;
  
  std::string cssPath = "styles/" + name + ".css";
  std::string jsPath = "scripts/" + name + ".js";
  
  if(isFile(cssPath)){
    element << "\t\t\t\"css\": [\"" << cssPath << "\"]," << std::endl;
  }
  if(isFile(jsPath)){
    element << "\t\t\t\"js\": [\"" << jsPath << "\"]," << std::endl;
  }
  element << "\t\t\t\"run_at\": \"document_end\"" << std::endl;
  element << "\t\t}";
  
  return element.str();
}

std::string Website::printBackgroundScripts(void) { 
  std::string jsPath = "scripts/background/" + name + ".js";
  
  if(isFile(jsPath)){
    return "\"" + jsPath + "\", ";
  }
  return "";
}

std::string Website::printComments(std::map<std::string, std::string> &manifesto) { // Used to fill CHANGES.md
  std::stringstream comments;
  
  std::vector<std::string> filePaths;
  filePaths.push_back("styles/" + name + ".css");
  filePaths.push_back("scripts/" + name + ".js");
  filePaths.push_back("scripts/background/" + name + ".js");
  
  for (auto const& filePath: filePaths) {
    if(isFile(filePath)){
      std::ifstream file(filePath);
      std::string line;
      int lineNumber = 0;
      while (std::getline(file, line)) {
      	lineNumber ++;
        size_t commentStart;
        std::string startString = "/* - ";
        size_t commentEnd;
        std::string endString = " */";
        if (
          (commentStart = line.find(startString, 0)) != std::string::npos &&
          (commentEnd = line.find(endString, 0)) != std::string::npos
        ) {
          commentStart += startString.length();
          std::vector<std::string> commentParts = split(
            line.substr(commentStart, commentEnd - commentStart), " - "
          );
          
          comments << "<details>" << std::endl << "<summary>";
          comments << capitalize(commentParts[0]);
          comments << "</summary>" << std::endl << std::endl;
          
          int rulesFound = 0;
          if(commentParts.size() > 1) {
            std::vector<std::string> tags;
            if(commentParts[1].find(" ", 0) != std::string::npos) {
              tags = split(commentParts[1], " ");
            } else {
              tags.push_back(commentParts[1]);
            }
            
            for (auto const& tag: tags) {
              if(manifesto.find(tag) != manifesto.end()) {
                comments << "> « " << manifesto[tag] << " »  " << std::endl;
                rulesFound++;
              }
            }
            if(rulesFound > 0){
              comments << "> *From the [manifesto](MANIFESTO.md).*  " << std::endl;
            }
          }
          if(rulesFound == 0) {
            comments << "> No existing rule from the [manifesto](MANIFESTO.md) has been defined for this change.  " << std::endl;
          }
          comments << "> ___ " << std::endl;
          comments << "> This change is implemented in [" << filePath << "](" << filePath << ")";
          comments << " at [line " << lineNumber << "](" << filePath << "#L" << lineNumber << ").";
          comments << std::endl << std::endl << "</details>" << std::endl;
        }
      }
      file.close();
    }
  }
  
  return comments.str();
}

