#include "functions.h"
#include <map>

class Website {
  private:
    std::string name;
    std::vector<std::string> tlds;
  public:
    Website(void);
    Website(std::string name);
    void setName(std::string newName);
    std::string getName(void);
    void addTld(std::string tld);
    std::string printUrls(void);
    std::string printElement(void);
    std::string printBackgroundScripts(void);
    std::string printComments(std::map<std::string, std::string> &manifesto);
};
