#include <string>
#include <vector>
#include <fstream>
#include <sstream>

std::vector<std::string> split(const std::string stringToSplit, const std::string delimiter);
void replace(std::string& stringToParse, const std::string stringToFind, const std::string newString);
bool isFile(const std::string path);
std::string capitalize(std::string stringToCap);
