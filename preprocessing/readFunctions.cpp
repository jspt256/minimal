#include "readFunctions.h"

std::map<std::string, std::string> readManifesto(std::string manifestoPath){
  std::ifstream manifestoFile(manifestoPath);
  std::string manifestoLine;
  std::map<std::string, std::string> manifesto;
  std::string h2 = "";
  int h3 = 0;
  std::string buffer;
  
  std::string tag = "0";
  std::string previousTag = "0";
  
  while (std::getline(manifestoFile, manifestoLine)) {
    int startString = 0;
    std::string beginString = "\n";
    
    if (manifestoLine.find("### ", 0) != std::string::npos){
		  h3 ++;
      startString = 7;
		  beginString = "";
    } else if (manifestoLine.find("## ", 0) != std::string::npos){
		  h2 = manifestoLine[3];
      h3 = 0;
    }
    if(h3 > 0){
      previousTag = tag;
      tag = h2 + std::to_string(h3);
      
      if(previousTag != tag && buffer.size() > 0){
        manifesto[previousTag] = buffer;
        buffer = "";
      }
      if(manifestoLine.size() > 0){
        buffer += beginString + manifestoLine.substr(startString);
      }
  	}
  }
  
  manifesto[previousTag] = buffer;
  
  return manifesto;
}

std::string readVersion(std::string versionPath){
  std::ifstream versionFile(versionPath);
  std::string version;
  std::getline(versionFile, version);
  
  return version;
}

std::vector<Website> readSupportedWebsites(std::string supportedWebsitesPath){
  std::ifstream supportedWebsitesFile(supportedWebsitesPath);
  std::string websiteLine;
  std::getline(supportedWebsitesFile, websiteLine); // Drop first line
  std::vector<Website> websites;
  
  while (std::getline(supportedWebsitesFile, websiteLine)) {
    std::vector<std::string> websiteInfo = split(websiteLine, ",");
    std::vector<std::string> websiteTlds = split(websiteInfo[1], " ");
    websites.push_back(websiteInfo[0]);
    
    for (auto const& tld: websiteTlds) {
      websites.back().addTld(tld);
    }
  }
  return websites;
}
