
/* - Remove the trending page - P2 */
browser.webRequest.onBeforeRequest.addListener(
function(requestDetails){
	return {redirectUrl: "https://www.youtube.com/feed/subscriptions"};
},
{
	urls: ["*://www.youtube.com/feed/trending*"],
	types: ["main_frame"]
},
["blocking"]
);

/* - Remove autoplay "feature" - U1 */
browser.webRequest.onBeforeRequest.addListener(
function(requestDetails){
	return {cancel: true};
},
{
	urls: ["https://www.youtube.com/yts/jsbin/*/endscreen.js"],
	types: ["script"]
},
["blocking"]
);


